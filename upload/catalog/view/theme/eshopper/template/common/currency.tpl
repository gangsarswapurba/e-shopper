<?php if (count($currencies) > 1) { ?>
<div class="pull-left">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="currency">
  <div class="btn-group">
    <?php  
    $currency_title = '';
    $currency_code = ''; 
    $currency_symbol = '';
    foreach ($currencies as $currency) { 
        $currency_title = $currency['title'];
        $currency_code = strtolower($currency['code']);
        $currency_symbol = ($currency['symbol_left'] && $currency['code']) == $code ? $currency['symbol_left'] : $currency['symbol_right']; 
    } 
    ?>
    <button class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
    <?php echo $currency_symbol; ?> <span class="hidden-xs hidden-sm hidden-md"><?php echo $currency_title; ?></span> <i class="fa fa-caret-down"></i>
    </button>
    <ul class="dropdown-menu">
      <?php foreach ($currencies as $currency) { ?>
      <?php if ($currency['symbol_left']) { ?>
      <li><a class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?> <?php echo $currency['title']; ?></a></li>
      <?php } else { ?>
      <li><a class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_right']; ?> <?php echo $currency['title']; ?></a></li>
      <?php } ?>
      <?php } ?>
    </ul>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>
							