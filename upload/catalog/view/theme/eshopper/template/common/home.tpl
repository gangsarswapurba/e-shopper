<?php echo $header; ?>
<section>
    <div class="container">
      <div class="row">
        <?php echo $column_left; ?>
        <?php $class = $column_left && $column_right ? 'col-sm-6' : $column_left || $column_right ? 'col-sm-9 padding-right' : 'col-sm-12'; ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?><?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?>
      </div>
    </div>
</section>
<?php echo $footer; ?>