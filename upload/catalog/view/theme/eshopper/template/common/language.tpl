<?php if (count($languages) > 1) { ?>
<div class="pull-left">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
  <div class="btn-group">
    <?php 
    $language_name = '';
    $language_code = '';
    $language_image = '';
    foreach ($languages as $language) {
        if ($language['code'] == $code) {
            $language_name = $language['name'];
            $language_code = $language['code'];
            $language_image = $language['image']; 
        }
    } ?>
    <button class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
    <img src="image/flags/<?php echo $language_image; ?>" alt="<?php echo $language_name; ?>" title="<?php echo $language_name; ?>" /> <span class="hidden-xs hidden-sm hidden-md"><?php echo $language_name; ?></span> <i class="fa fa-caret-down"></i>
    </button>
    <ul class="dropdown-menu">
      <?php foreach ($languages as $language) { ?>
      <li><a href="<?php echo $language['code']; ?>"><img src="image/flags/<?php echo $language['image']; ?>" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <input type="hidden" name="code" value="" />
  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
</form>
</div>
<?php } ?>
							