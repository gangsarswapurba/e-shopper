<div class="shop-menu pull-right">
    <ul class="nav navbar-nav">
		<li><a href="#"><i class="fa fa-user"></i> <?php $text_account; ?></a></li>
		<li><a href="#"><i class="fa fa-star"></i> <?php  $text_wishlist; ?></a></li>
		<li><a href="checkout.html"><i class="fa fa-crosshairs"></i> Checkout</a></li>
		<li><a href="cart.html"><i class="fa fa-shopping-cart"></i> Cart</a></li>
		<li><a href="login.html"><i class="fa fa-lock"></i> Logout</a></li>
	</ul>
</div>