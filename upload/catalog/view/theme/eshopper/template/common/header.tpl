<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="<?php echo $lang; ?>" dir="<?php echo $direction; ?>">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <?php if ($base) {?>
        <base href="<?php echo $base; ?>">
    <?php }?>
    <?php if ($description) {?>
        <meta name="description" content="<?php echo $description; ?>">
    <?php }?>
    <?php if ($keywords) {?>
        <meta name="keywords" content="<?php echo $keywords; ?>">;
    <?php }?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="catalog/view/theme/eshopper/css/bootstrap.min.css" rel="stylesheet">
    <link href="catalog/view/theme/eshopper/css/font-awesome.min.css" rel="stylesheet">
    <link href="catalog/view/theme/eshopper/css/prettyPhoto.css" rel="stylesheet">
    <link href="catalog/view/theme/eshopper/css/price-range.css" rel="stylesheet">
    <link href="catalog/view/theme/eshopper/css/animate.css" rel="stylesheet">
	<link href="catalog/view/theme/eshopper/css/main.css" rel="stylesheet">
	<link href="catalog/view/theme/eshopper/css/responsive.css" rel="stylesheet">
	<?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <?php if ($icon) {?>
        <link href="<?php echo $icon; ?>" rel="shortcut icon" />
    <?php }?>
    <!-- 
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="catalog/view/theme/eshopper/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="catalog/view/theme/eshopper/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="catalog/view/theme/eshopper/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="catalog/view/theme/eshopper/images/ico/apple-touch-icon-57-precomposed.png">
     -->
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
</head><!--/head-->

<body class="<?php echo $class; ?>">
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
							    <?php if ($no_telp) {?>
							        <li><a href="#"><i class="fa fa-phone"></i> <?php echo $no_telp; ?></a></li>
							    <?php }?>
								<?php if ($email) {?>
								    <li><a href="#"><i class="fa fa-envelope"></i> <?php echo $email; ?></a></li>
								<?php }?>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
							    <?php if ($facebook) {?>
							        <li><a href="https://www.facebook.com/<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a></li>
							    <?php }?>
								<?php if ($twitter) {?>
								    <li><a href="https://www.twitter.com/<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a></li>
								<?php }?>
								<?php if ($linkedin) {?>
								    <li><a href="https://www.linkedin.com/<?php echo $linkedin; ?>"><i class="fa fa-linkedin"></i></a></li>
								<?php }?>
								<?php if ($dribbble) {?>
								    <li><a href="https://www.dribbble.com/<?php echo $dribbble; ?>"><i class="fa fa-dribbble"></i></a></li>
								<?php }?>
								<?php if ($google_plus) {?>
								    <li><a href="https://www.google.com/<?php echo $google_plus; ?>"><i class="fa fa-google-plus"></i></a></li>
								<?php }?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
					    <div class="logo pull-left">
					    <?php if ($image_logo_url) {?>
    					    <a href="<?php echo $base; ?>"><img src="<?php echo $image_logo_url; ?>" alt="" /></a>
					    <?php }?>
					    </div>
						<div class="btn-group pull-right">
							<?php echo $language; ?>
							
							<?php echo $currency; ?>
						</div>
					</div>
					<div class="col-sm-6">
					    <?php if ($logged) {
					        echo $account_menu_logged_in;;
					    } else {
					        echo $account_menu;
					    }?>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<?php echo $main_menu; ?>
					</div>
					<div class="col-sm-3">
						<?php echo $search; ?>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->