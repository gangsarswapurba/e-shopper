<?php

class ControllerEshopperHome extends Controller {

  public function index() {
    $this->load->model('setting/setting');
    
    $data = array();
    
    $data += $this->load->language('eshopper/home');
    
    $this->document->setTitle($this->language->get('heading_title'));
    $this->document->addStyle('view/stylesheet/eshopper/general.css');
    
    $data['heading_title'] = $this->language->get('heading_title');
    
    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
      $this->model_setting_setting->editSetting('eshopper', $this->request->post);
      
      $this->session->data['success'] = $this->language->get('text_success');
      
      // $this->response->redirect($this->url->link('eshopper/home', 'token=' . $this->session->data['token'], 'SSL'));
    }
    
    $data['breadcrumbs'] = array();
    
    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('text_home'),
      'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
    );
    
    $data['breadcrumbs'][] = array(
      'text' => $this->language->get('heading_title'),
      'href' => $this->url->link('eshopper/home', 'token=' . $this->session->data['token'], 'SSL')
    );
    
    $data['token'] = $this->session->data['token'];
    
    if (isset($this->error['warning'])) {
      $data['error_warning'] = $this->error['warning'];
    } else {
      $data['error_warning'] = '';
    }
    
    if (isset($this->session->data['success'])) {
      $data['success'] = $this->session->data['success'];
      
      unset($this->session->data['success']);
    } else {
      $data['success'] = '';
    }
    
    $theme_setting = $this->model_setting_setting->getSetting('eshopper');
    
//     if (isset($this->request->post['eshopper_responsive'])) {
//       $data['eshopper_responsive'] = $this->request->post['eshopper_responsive'];
//     } else {
//       $data['eshopper_responsive'] = $theme_setting['eshopper_responsive'];
//     }
    
    $data['action'] = $this->url->link('eshopper/home', 'token=' . $this->session->data['token'], 'SSL');
    
    $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL');
    
    $data['token'] = $this->session->data['token'];
    
    $data['header'] = $this->load->controller('common/header');
    $data['column_left'] = $this->load->controller('common/column_left');
    $data['footer'] = $this->load->controller('common/footer');
    
    $this->response->setOutput($this->load->view('eshopper/home.tpl', $data));
  }

  protected function validate() {
    if (! $this->user->hasPermission('modify', 'eshopper/home')) {
      $this->error['warning'] = $this->language->get('error_permission');
    }
    ;
    
    if ($this->error && ! isset($this->error['warning'])) {
      $this->error['warning'] = $this->language->get('error_warning');
    }
    
    return ! $this->error;
  }
}
