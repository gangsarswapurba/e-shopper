
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-tema-buat-latihan" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-tema-buat-latihan" class="form-horizontal">
          <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#tab-home" data-toggle="tab"><span class="glyphicon glyphicon-home"></span> <?php echo $tab_home; ?></a></li>
            <li><a href="#tab-setting" data-toggle="tab"><span class="glyphicon glyphicon-cog"></span> <?php echo $tab_setting; ?></a></li>
            <li><a href="#tab-about" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> <?php echo $tab_about; ?></a></li>
          </ul>
          <div class="tab-content">
            <!-- Home -->
            <div class="tab-pane active" id="tab-home">
              <h1 class="semi-transparent">Welcome! This is Home</h1>
            </div>
            <!-- End Home -->
            <!-- Setting -->
            <div class="tab-pane" id="tab-setting">
              <div class="form-group">
                <div class="panel-body">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-setting-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                    <li><a href="#tab-setting-header" data-toggle="tab"><?php echo $tab_header; ?></a></li>
                    <li><a href="#tab-setting-footer" data-toggle="tab"><?php echo $tab_footer; ?></a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab-setting-general">
                      <h1 class="semi-transparent">Welcome! This is General Setting</h1>
                    </div>
                    <div class="tab-pane" id="tab-setting-header">
                      <div class="row">
                        <div class="col-sm-6">
                          <fieldset>
                            <legend>Contact</legend>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your phone number">Phone Number</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_contact_phone" class="form-control" type="text" name="eshopper_contact_phone"></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your email">Email</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_contact_email" class="form-control" type="text" name="eshopper_contact_email"></input>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                        <div class="col-sm-6">
                          <fieldset>
                            <legend>Social Media</legend>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your Facebook username">Facebook</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_socmed_facebook" class="form-control" type="text" name="eshopper_socmed_facebook"></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your Twitter username">Twitter</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_socmed_twitter" class="form-control" type="text" name="eshopper_socmed_twitter"></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your LinkedIn username">LinkedIn</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_socmed_linkedin" class="form-control" type="text" name="eshopper_socmed_linkedin"></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your Dribbble username">Dribbble</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_socmed_dribbble" class="form-control" type="text" name="eshopper_socmed_dribbble"></input>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your Google+ username">Google+</span>
                              </label>
                              <div class="col-sm-8">
                                <input id="input_socmed_google+" class="form-control" type="text" name="eshopper_socmed_google+"></input>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="tab-setting-footer">
                      <div class="row">
                        <div class="col-sm-6">
                          <fieldset>
                            <legend>Custom HTML Text - Columnt Left</legend>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your desired text (HTML accepted)">Custom HTML Text Left</span>
                              </label>
                              <div class="col-sm-8">
                                <textarea name="eshopper_custom_html_left" rows="5" placeholder="Copyright © 2013 E-SHOPPER Inc. All rights reserved" id="input-custom-html-left" class="form-control"></textarea>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                        <div class="col-sm-6">
                          <fieldset>
                            <legend>Custom HTML Text - Column Right</legend>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">
                                <span title="" data-toggle="tooltip" data-original-title="Fill with your desired text (HTML accepted)">Custom HTML Text Right</span>
                              </label>
                              <div class="col-sm-8">
                                <textarea name="eshopper_custom_html_right" rows="5" placeholder='Designed by Themeum' id="input-meta-description" class="form-control"></textarea>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Setting -->
            <!-- About -->
            <div class="tab-pane" id="tab-about">
              <h1 class="semi-transparent">About</h1>
            </div>
            <!-- End About -->
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>