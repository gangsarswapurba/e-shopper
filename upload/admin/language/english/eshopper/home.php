<?php
// Heading
$_['heading_title']                 = 'E-Shopper';

// Tab
$_['tab_home']                      = 'Home';
$_['tab_setting']                   = 'Setting';
$_['tab_about']                     = 'About';
$_['tab_general']                   = 'General';
$_['tab_header']                    = 'Header';
$_['tab_footer']                    = 'Footer';

// Text
$_['text_form']                     = 'Edit Preferences';
$_['text_success']                  = 'Success: You have modified theme preferences succesfully!';

// Entry
$_['entry_responsive']              = 'Responsive Mode';

// Help
$_['help_responsive']               = 'Enable/Disable Responsive mode';

// Error
$_['error_permission']              = 'You don\'t have a sufficient privileges to modifiy this theme. Sorry!';
$_['error_warning']                 = 'Warning: Please check the form carefully for errors!';

